<#
    Google Book Mark Sync Script
    -syncdirection up
        Sends copy to Homedrive

    -syncdirection down
        Brings copy down from home drive
#>
Function Get-FlatChromeBookMarks 
{
    param($Bookmarks, $bookmarkfoldername)

    $BookmarkObjects = @()
    Foreach($bookmark in $Bookmarks)
    {
        If($bookmark.type -eq "folder")
        {
            $tempbookmarkfolder = "" | Select children, date_added, date_modified, id, name, type, _FolderName
            
            $tempbookmarkfolder.date_added = $bookmark.date_added
            $tempbookmarkfolder.date_modified = $bookmark.date_modified
            $tempbookmarkfolder.id = ""#$bookmark.id
            $tempbookmarkfolder.name = $bookmark.name
            $tempbookmarkfolder.type = $Bookmark.type
            $tempbookmarkfolder._FolderName = $bookmarkfoldername
            $BookmarkObjects += Get-FlatChromeBookMarks -Bookmarks $bookmark.children -bookmarkfoldername $bookmark.name
            $BookmarkObjects += $tempbookmarkfolder

        }
        elseif($bookmark.type -eq "url")
        {
            $tempbookmark = "" | select date_added, id, name, type, url, _FolderName

            $tempbookmark.date_added = $bookmark.date_added
            $tempbookmark.id = ""#$Bookmark.id
            $tempbookmark.name = $bookmark.name
            $tempbookmark.type = $Bookmark.type
            $tempbookmark.url = $Bookmark.url
            $tempbookmark._FolderName = $bookmarkfoldername
            $BookmarkObjects += $tempbookmark
            
        }

    }

    return $BookmarkObjects;
}

Function Build-ChromeBookMarks
{
    param($Bookmarks, $bookmarkfoldername)
    
    ##Grab This Level
    $_Bookmarks = $Bookmarks.where{$_._Foldername -eq $bookmarkfoldername}
    $BookmarkObjects = @()
    foreach($bookmark in $_Bookmarks)
    {
        If($bookmark.type -eq "folder")
        {
      
            $tempbookmarkfolder = "" | Select children, date_added, date_modified, id, name, type
              
            $tempbookmarkfolder.date_added = $bookmark.date_added
            $tempbookmarkfolder.date_modified = $bookmark.date_modified
            $tempbookmarkfolder.id = ""#$bookmark.id
            $tempbookmarkfolder.name = $bookmark.name
            $tempbookmarkfolder.type = $Bookmark.type
            $tempbookmarkfolder.children = Build-ChromeBookMarks -Bookmarks $Bookmarks -bookmarkfoldername $bookmark.name
            $BookmarkObjects += $tempbookmarkfolder

        }
        elseif($bookmark.type -eq "url")
        {
            $tempbookmark = "" | select date_added, id, name, type, url

            $tempbookmark.date_added = $bookmark.date_added
            $tempbookmark.id = ""#Bookmark.id
            $tempbookmark.name = $bookmark.name
            $tempbookmark.type = $Bookmark.type
            $tempbookmark.url = $Bookmark.url
            $BookmarkObjects += $tempbookmark
            
        }

        

    }
    
    return $BookmarkObjects;


}


function Sync-MHGooleBookmars
{

[CmdletBinding()]
param(
[switch]$sync,
[switch]$save
)

##Sync Users Data With Remote.
if($sync)
{
    ##Gather the Remote Resources

    $Remote_Folderpath = "\\Fileshare\\home\" + $env:USERNAME + "\ChromeSync\"
    Write-Verbose $Remote_Folderpath
    $Remote_GoogleChromeBookMarks = $Remote_Folderpath + "Synced_BookMarks"
    Write-Verbose $Remote_GoogleChromeBookMarks

    if(!(Test-Path $Remote_Folderpath))
    {
        Write-Verbose "Creating Remote Folder Location..."
        New-Item -ItemType Directory -Path $Remote_Folderpath 

        Write-Verbose "New Chrome Bookmark sync Folder Created..." -ForegroundColor Green
    }
    elseif(Test-Path $Remote_GoogleChromeBookMarks)
    {
        Write-Verbose "Getting Contenet from Remote Bookmark file."
        $Remote_GoogleChromeBookmarks_JSON = Get-Content $Remote_GoogleChromeBookMarks | ConvertFrom-Json 

    }


    ##Gather Local Resources
    $Local_GoogleChromeBookmarks = $env:USERPROFILE + '\AppData\Local\Google\Chrome\User Data\Default\Bookmarks'
    Write-Verbose $Local_GoogleChromeBookmarks
    if(Test-Path $Local_GoogleChromeBookmarks)
    {
    $Local_GoogleChromeBookmarks_JSON = Get-Content $Local_GoogleChromeBookmarks | ConvertFrom-Json 
    }
    else
    {
    Write-Verbose "No BookMark File... Creating"
     $BookmarksTemplate = @"

        {
   
           "roots": {
              "bookmark_bar": {
                 "children": [ ],
                 "date_added": "13193289771592410",
                 "date_modified": "13193289774010921",
                 "id": "1",
                 "name": "Bookmarks bar",
                 "type": "folder"
              },
              "other": {
                 "children": [  ],
                 "date_added": "13193289771592421",
                 "date_modified": "0",
                 "id": "2",
                 "name": "Other bookmarks",
                 "type": "folder"
              },
              "synced": {
                 "children": [  ],
                 "date_added": "13193289771592423",
                 "date_modified": "0",
                 "id": "3",
                 "name": "Mobile bookmarks",
                 "type": "folder"
              }
           },
           "version": 1
        }


"@
    $BookmarksTemplate | Out-File $Local_GoogleChromeBookmarks
        
    }


    ##SendBackup to Home Drive
    Write-Verbose "Copying chrome bookmark backups"
    Copy-Item $Local_GoogleChromeBookmarks ($Remote_GoogleChromeBookMarks + ".bak") -Force

    Write-Verbose "Get Flat bookmarks for local..."
    $Local_FlatBookmarks = Get-FlatChromeBookMarks -Bookmarks $Local_GoogleChromeBookmarks_JSON.roots.bookmark_bar.children -bookmarkfoldername bookmark_bar
    Write-Verbose "Number of Bookmarks $( $Local_FlatBookmarks.count) " 

    Write-Verbose "Get Flat bookmarks for remote.."
    $Remote_Flatbookmarks = Get-FlatChromeBookMarks -Bookmarks $Remote_GoogleChromeBookmarks_JSON.roots.bookmark_bar.children -bookmarkfoldername bookmark_bar
    Write-Verbose "Number of Bookmarks $($Remote_Flatbookmarks.count)" 
     if($Local_FlatBookmarks.count -eq 0)
    {
       Write-Verbose "No Local Bookmarks Copying from Remote"
       Copy-Item $Remote_GoogleChromeBookMarks $Local_GoogleChromeBookmarks -Force
    }
    else
    {
        Write-Verbose "Compareing Local to Remote"
        $diffs = compare $Local_FlatBookmarks $Remote_Flatbookmarks -Property Name,Url
        if($diffs -ne $null)
        {
            Write-Verbose "Changes found Building new bookmark file."
            #Complie the new bookmark bar
            $NewBookmarks = @()
            $NewBookmarks += $Local_FlatBookmarks
            $NewBookmarks += foreach($diff in $diffs){$Remote_Flatbookmarks.where{$_.url -eq $diff.url}}

    
            $Bookmark_bar = Build-ChromeBookMarks -Bookmarks $NewBookmarks -bookmarkfoldername bookmark_bar

            $BookmarksTemplate = @"

            {
   
               "roots": {
                  "bookmark_bar": {
                     "children": [ ],
                     "date_added": "13193289771592410",
                     "date_modified": "13193289774010921",
                     "id": "1",
                     "name": "Bookmarks bar",
                     "type": "folder"
                  },
                  "other": {
                     "children": [  ],
                     "date_added": "13193289771592421",
                     "date_modified": "0",
                     "id": "2",
                     "name": "Other bookmarks",
                     "type": "folder"
                  },
                  "synced": {
                     "children": [  ],
                     "date_added": "13193289771592423",
                     "date_modified": "0",
                     "id": "3",
                     "name": "Mobile bookmarks",
                     "type": "folder"
                  }
               },
               "version": 1
            }

"@
            Write-Verbose "Converting Powershell Object to bookmark file"
            $BookMarksTemplateJSON = $BookmarksTemplate | ConvertFrom-Json
            $BookMarksTemplateJSON.roots.bookmark_bar.children = $Bookmark_bar
            Write-Verbose "Saving New Bookmark File"
            $BookMarksTemplateJSON | ConvertTo-Json -Depth 20 | Out-File -Encoding utf8 -FilePath $Local_GoogleChromeBookmarks
            Sync-MHGooleBookmars -save
        }
        else
        {
            Write-Verbose "No Changes Found"
        }


    }

}


    if($save)
    {
        $Local_GoogleChromeBookmarks = $env:USERPROFILE + '\AppData\Local\Google\Chrome\User Data\Default\Bookmarks'
        $Remote_Folderpath = "\\Fileshare\\home\" + $env:USERNAME + "\ChromeSync\"
        $Remote_GoogleChromeBookMarks = $Remote_Folderpath + "Synced_BookMarks"

        if(!(Test-Path $Remote_Folderpath))
        {
            Write-Verbose $Remote_Folderpath
            
            Write-Verbose $Remote_GoogleChromeBookMarks

            Write-Verbose "Creating Remote Folder Location..."
            New-Item -ItemType Directory -Path $Remote_Folderpath 
            
            Write-Verbose "Copying Item to HomeDrive..."
            Copy-Item $Local_GoogleChromeBookmarks $Remote_GoogleChromeBookMarks -Force

        }
        elseif(Test-Path $Remote_GoogleChromeBookMarks)
        {
            Write-Verbose "Copying Item to HomeDrive..."
            Copy-Item $Local_GoogleChromeBookmarks $Remote_GoogleChromeBookMarks -Force

        }


    }


}


#Sync-MHGooleBookmars -save -Verbose

#Sync-MHGooleBookmars -sync -Verbose